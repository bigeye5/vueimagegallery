# VueImageGallery

## Dependencies
Either update .env with your Imgur client id or create a .env.local with the same. This is a free service. If you don't have an account register one on https://imgur.com 
Then on https://apidocs.imgur.com scroll down on the home page and find the registration link. NOTE: when creating the api client ensure that the callback url is /oauth2/callback , alternatively edit main.js with the callback route specified during api client creation.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
