import imgur from '../../api/imgur';
import qs from 'qs';
import { router } from '../../main';

const state = {
    token: window.localStorage.getItem('imgur_token')
};

const getters = {
    isLoggedIn: state => !!state.token 

    // above is short-hand for
    // isLoggedIn: (state) => {
    //     return !!state.token // short-hand to return boolean value of something
    // }
};

const actions = {
    login: () => {
        imgur.login();
    },
    finalizeLogin({ commit }, hash) {
        // create key value set
        const query = qs.parse(hash.replace('#', ''));
        commit('setToken', query.access_token);
        window.localStorage.setItem('imgur_token', query.access_token);
        router.push('/');
    },
    logout: ({ commit }) => {
        // mutations.setToken // bad to directly call mutation!! 
        // rather use commit function to call mutations
        commit('setToken', null);
        window.localStorage.removeItem('imgur_token');
    }
};

const mutations = {
    setToken: (state, token) => {
        state.token = token;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}