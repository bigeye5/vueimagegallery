import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import store from './store';
import AuthHandler from './components/AuthHandler.vue';
import ImageList from './components/ImageList.vue';
import UploadForm from './components/UploadForm.vue';


Vue.use(VueRouter);
export const router = new VueRouter({
    mode: 'history', // change default config to use browser router iso # router, nb for 3rd party integrations
    routes: [
        {path: '/oauth2/callback', component: AuthHandler}, // ensure component is imported above
        {path: '/', component: ImageList},
        {path: '/upload', component: UploadForm}
    ]
})

new Vue({
    router, // add router instance to vue instance
    store, // make Vuex available to Vue, note: also need to: Vue.use(Vuex); in store/index.js
    render: h => h(App)
}).$mount('#app');